const express = require('express');
const mongoose = require('mongoose');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const jwt = require('express-jwt');
var secret = "!@#DWe$%^gge&&**";
const path = require('path');
const fileRoute = require('./controllers/file');

const registerController = require('./controllers/registrationController');
const loginController = require('./controllers/loginController');

mongoose.connect('mongodb://localhost/wisdom_test_database', {
    useNewUrlParser: true,
    useUnifiedTopology: true
},(err,success)=>{
    console.log("connection successfully created");
});

//Serves all the request which includes /images in the url from Images folder
app.use('/images', express.static(__dirname + '/Images'));

app.use(express.static(path.join(__dirname, '..', 'build')));
// app.use(fileRoute);

// app.get('*', (req, res) => {
//     res.sendFile(path.join(__dirname, '..', 'build'));
//   });


app.use(cors({
    origin: function(origin, callback){
      return callback(null, true);
    },
    optionsSuccessStatus: 200,
    credentials: true
  }));
app.use('/app', fileRoute);


app.use('/api', jwt({ secret: secret,algorithms:['RS256'] }).unless({ path: ['/register', '/login'] }));
app.use(cors());
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json());
app.use('/register', registerController);
app.use('/login', loginController);
app.listen(5000, () => {
    console.log('server connected successfully')
})