const express = require('express');
var router = express.Router();
const registerService = require('./../services/registrationService');
router.post('/', registration);

function registration(req, res) {
    let body = req.body;
    registerService.Register(body).then((data, err) => {
        if (err) {
            res.send(err).status(400)
        } else {
            res.send({msg:"user successfully registered",user:data});
        }
    })
}

module.exports = router;