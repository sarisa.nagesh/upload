const path = require('path');
const express = require('express');
var fs = require('fs');
const multer = require('multer');
const File = require('../schemas/file');
const Router = express.Router();
const DIR = './images';


if (!fs.existsSync(DIR)){
  fs.mkdirSync(DIR);
}


let storage = multer.diskStorage({

destination: (req, file, cb) => {
  let fieldname=file.fieldname.split("|");
  let file_path=DIR;
  //let file_path=DIR

  if (!fs.existsSync(file_path)){
    fs.mkdirSync(file_path);
    cb(null, file_path);
}else{
    cb(null, file_path);
    }
},
filename: (req, file, cb) => {
  //console.log("file.fieldname====>",file.fieldname);
  // cb(null,file.originalname);
  
  cb(null, 'wisdomdoc' + '-' + Date.now() + file.originalname);
}
});

let Upload = multer({storage: storage})

Router.post('/UploadFile',Upload.any(), UploadFile);

async function UploadFile(req, res, next) {
  var response = req.files
  if (!req.files[0].fieldname) {
    console.log("No file received");
    return res.send({
      success: false
    });    
  } else {
        // console.log('file received',response);
        console.log("req.files[0].fieldname===>",req.files[0].fieldname);
      try {
        const { title, description } = req.body;
        const { path, mimetype } = response[0];
        const file = new File({
          title,
          description,
          file_path: path,
          file_mimetype: mimetype
        });
//{"fileName":"saml_assertion_req.PNG","uploaded":1,"url":"https:\/\/ckeditor.com\/apps\/ckfinder\/userfiles\/images\/saml_assertion_req.PNG"}
        await file.save();
        // res.send(response);
        res.send({"fileName":response[0].originalname,"uploaded":1,"url":`http:\/\/localhost:5000\/${path}`});

      } catch (error) {
        res.status(400).send('Error while uploading file. Try again later.');
      }
      }
}






Router.get('/getAllFiles', async (req, res) => {
  try {
    const files = await File.find({});
    const sortedByCreationDate = files.sort(
      (a, b) => b.createdAt - a.createdAt
    );
    res.send(sortedByCreationDate);
  } catch (error) {
    res.status(400).send('Error while getting list of files. Try again later.');
  }
});

// Router.get('/download/:id', async (req, res) => {
  Router.get('/getById', async (req, res) => {
  try {
    const file = await File.findById(req.params.id);
    res.set({
      'Content-Type': file.file_mimetype
    });
    res.sendFile(path.join(__dirname, '..', file.file_path));
  } catch (error) {
    res.status(400).send('Error while downloading file. Try again later.');
  }
});

module.exports = Router;